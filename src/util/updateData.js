import data from "../data/stateData";
console.log("gfddfghjkjhgf", data);
const updateData = (qid, todos) => {
  let updatedTodos = [];
  switch (qid) {

    case "Q0":
      todos[todos.findIndex(todo => todo.id === 9)].title = "Hello World";
      updatedTodos = [...todos];
      break;

    case "Q1":
      todos[todos.findIndex(todo => todo.id === 9)].title = "Hello World";
      updatedTodos = [...todos];
      break;

    case "Q2":
      updatedTodos = [...todos.filter(todo => todo.userId !== 3)];
      break;

    case "Q3":
      updatedTodos = [...todos, {
        userId: 2,
        "id": 11,
        "title": "Quadratic Equations",
        "completed": false
      }]
      break;

    case "Q4":
      todos.splice(5, 0, {
        userId: 2,
        "id": 12,
        "title": "Differential Equations",
        "completed": false
      });
      updatedTodos = [...todos];
      break;

    case "Q5":
      const itemOne = { ...todos[2] };
      const itemTwo = { ...todos[7] };
      todos[2] = itemTwo;
      todos[7] = itemOne;
      updatedTodos = [...todos];
      break;


    case "Q6":
      todos.splice(3, 1);
      updatedTodos = [...todos];
      break;

    case "Q7":
      const array = [{
        userId: 2,
        "id": 13,
        "title": "Kirchoffs Equations",
        "completed": false
      }, {
        userId: 1,
        "id": 14,
        "title": "Kepler's Equations",
        "completed": false
      }];
      todos.splice(2, 0, ...array);
      updatedTodos = [...todos];
      break;

    case "Q8":
      delete todos[6].title;
      updatedTodos = [...todos];
      break;

    case "Q9":
      let itemAtTwo = { ...todos[1] };
      let itemAtSix = { ...todos[5] };
      const auxItem = { ...itemAtTwo };
      updatedTodos = [...todos];
      break;
    default: ;

  }
  return updatedTodos;
}
export default updateData;