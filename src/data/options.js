const options = [
  { value: "Q0", label: "Initial State" },
  { value: "Q1", label: "Q1. Change Title of the element to Hello World with id = 9 " },
  { value: "Q2", label: "Q2. Delete all items with userId - 3" },
  { value: "Q3", label: "Q3. Add a new Element at the end of the array" },
  { value: "Q4", label: "Q4. Add a new Element at position 6" },
  { value: "Q5", label: "Q5. Swap elements at position 3 and 8 " },
  { value: "Q6", label: "Q6. Delete element at position 4" },
  { value: "Q7", label: "Q7. Add a new Array at position 2 " },
  { value: "Q8", label: "Q8. Delete title Property from element at position 7." },
  { value: "Q9", label: "Q9. Swap ids and titles  of elements at positions 2 and 6" },
];
export default options;