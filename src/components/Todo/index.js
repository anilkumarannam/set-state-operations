import './todo.css';
const Todo = (props) => {

  const { userId, id, title, completed } = props.todoItem;

  return <>
    <div className="todo">
      <p>User Id: {userId}</p>
      <p>Todo Id: {id}</p>
      <p>Title: {title}</p>
      <p>Status: {completed ? "Completed" : "Incomplete"}</p>
    </div>
  </>

}

export default Todo;