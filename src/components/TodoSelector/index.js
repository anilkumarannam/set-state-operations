import options from "../../data/options";

const TodoSelector = (props) => {
  return <>
    <select onChange={(event) => props.update(event.target.value)}>
      {options.map(option => <option key={option.value} value={option.value}>{option.label}</option>)}
    </select>
  </>
}

export default TodoSelector;
