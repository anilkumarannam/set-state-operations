import { Component } from "react";
import TodoSelector from "../TodoSelector";
import Todo from "../Todo";
import './todoContainer.css'
import data from "../../data/stateData";
import updateData from "../../util/updateData";

class TodoContainer extends Component {

  state = { todos: data }

  updateState = (qId) => {
    console.log(qId);
    const { todos } = this.state;
    const updatedTodos = updateData(qId, todos);
    console.log(todos);
    this.setState({ todos: updatedTodos });
  }

  render() {
    const { todos } = this.state;
    let keyValue = 1;
    return <>
      <div className="todo-container">
        <div>
          <TodoSelector update={this.updateState} />
        </div>
        <div className="todos">
          {todos.map((todoItem) => <Todo key={keyValue++} todoItem={todoItem} />)}
        </div>
      </div>
    </>
  }
}

export default TodoContainer;